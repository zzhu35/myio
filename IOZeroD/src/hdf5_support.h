/* 
    Author: Zeran Zhu
            National Center for Supercomputing Applications (NCSA), and
            Computer Engineering,
            University of Illinois at Urbana-Champaign (UIUC)
            zzhu35@ncsa.illinois.edu
            July 2018

    Mentor: Roland Haas
            NCSA,
            UIUC
            rhaas@ncsa.illinois.edu

hdf5_support.h:
    HDF5 file format support
    and related data structure
    declaration

*/

#ifndef _HDF5_SUPPORT_H_
#define _HDF5_SUPPORT_H_

#include "cctk.h"
#include "cctk_Arguments.h"
#include <hdf5.h>

#define MAX_DSET_NAME 256

typedef struct var_t
{
    struct var_t* next;
    int type; // 0 for ascii, 1 for hdf5
    int idx;
    void* buf;
    // size_t occupied;
} var_t;

typedef struct
{
    char* fname;
} h5file_t;

typedef struct h5dset_t
{
    struct h5dset_t* next;
    // hid_t did; // dataset id
    char* path;
    h5file_t* file;
    hsize_t cols; // number of total columns, including time
    hsize_t len;
    hid_t dtype;
    int cctk_type;
    void* tbuf; // buffer for timing data
    size_t occupied; // index for tbuf
    var_t* var_head;
    CCTK_REAL last_touched_time;
    int last_touched_epoch;
    CCTK_REAL output_every_time;
    int output_every_epoch;
} h5dset_t;

void h5append_attribute(hid_t loc, const char* name, const char* attrstring);
int h5dset_append(h5dset_t* in, void** buf, hsize_t len);
int h5file_add_dset(hid_t fileid, h5dset_t* ptr, hsize_t hdf5_chunk_size);
// from PUGH IOHDF5
hid_t iozerod_IOHDF5Util_DataType(int cctk_type);

/* get cctk variable from name */
#define CCTK_TYPE_FROM_NAME(name) (CCTK_VarTypeI(CCTK_VarIndex(name)))
#define CCTK_SIZE_FROM_NAME(name) (CCTK_VarTypeSize(CCTK_TYPE_FROM_NAME(name)))
#define CCTK_PTR_FROM_NAME(name) (CCTK_VarDataPtrI(cctkGH, 0, CCTK_VarIndex(name)))

/* get cctk variable from index */
#define CCTK_TYPE_FROM_IDX(index) (CCTK_VarTypeI(index))
#define CCTK_SIZE_FROM_IDX(index) (CCTK_VarTypeSize(CCTK_TYPE_FROM_IDX(index)))
#define CCTK_PTR_FROM_IDX(index) (CCTK_VarDataPtrI(cctkGH, 0, index))

/* get hdf5 variable type from name or index*/
#define HDF5_TYPE_FROM_NAME(name) (iozerod_IOHDF5Util_DataType(CCTK_TYPE_FROM_NAME(name)))
#define HDF5_TYPE_FROM_IDX(index) (iozerod_IOHDF5Util_DataType(CCTK_TYPE_FROM_IDX(index)))


#define H5CALL(stmt) do{ \
  hid_t retval = stmt;   \
  if (retval < 0) CCTK_VWARN(1, "H5 call \"" #stmt "\" returned error code %d", retval); \
} while(0);

/*** Define the different datatypes used for HDF5 I/O
     NOTE: the complex datatype is defined dynamically at runtime in Startup.c
 ***/
/* byte type is easy */
#define HDF5_BYTE   H5T_NATIVE_UCHAR

/* floating point types are architecture-independent,
   ie. a float has always 4 bytes, and a double has 8 bytes
     HDF5_REAL  is used for storing reals of the generic CCTK_REAL type
     HDF5_REALn is used to explicitely store n-byte reals */
#ifdef  HAVE_CCTK_REAL4
#define HDF5_REAL4  H5T_NATIVE_FLOAT
#endif
#ifdef  HAVE_CCTK_REAL8
#define HDF5_REAL8  H5T_NATIVE_DOUBLE
#endif
#ifdef  HAVE_CCTK_REAL16
#define HDF5_REAL16 (sizeof (CCTK_REAL16) == sizeof (long double) ?           \
                     H5T_NATIVE_LDOUBLE : -1)
#endif


#ifdef  CCTK_REAL_PRECISION_16
#define HDF5_REAL   HDF5_REAL16
#elif   CCTK_REAL_PRECISION_8
#define HDF5_REAL   HDF5_REAL8
#elif   CCTK_REAL_PRECISION_4
#define HDF5_REAL   HDF5_REAL4
#endif


/* integer types are architecture-dependent:
     HDF5_INT  is used for communicating integers of the generic CCTK_INT type
     HDF5_INTn is used to explicitely communicate n-byte integers */
#ifdef  HAVE_CCTK_INT8
#define HDF5_INT8   (sizeof (CCTK_INT8) == sizeof (int) ? H5T_NATIVE_INT :    \
                     sizeof (CCTK_INT8) == sizeof (long) ? H5T_NATIVE_LONG :  \
                     sizeof (CCTK_INT8) == sizeof (long long) ?               \
                     H5T_NATIVE_LLONG : -1)
#endif

#ifdef  HAVE_CCTK_INT4
#define HDF5_INT4   (sizeof (CCTK_INT4) == sizeof (int) ? H5T_NATIVE_INT :    \
                     sizeof (CCTK_INT4) == sizeof (short) ?                   \
                     H5T_NATIVE_SHORT : -1)
#endif

#ifdef  HAVE_CCTK_INT2
#define HDF5_INT2   (sizeof (CCTK_INT2) == sizeof (short) ?                   \
                     H5T_NATIVE_SHORT : -1)
#endif

#ifdef  HAVE_CCTK_INT1
#define HDF5_INT1   H5T_NATIVE_SCHAR
#endif

#ifdef  CCTK_INTEGER_PRECISION_8
#define HDF5_INT    HDF5_INT8
#elif   CCTK_INTEGER_PRECISION_4
#define HDF5_INT    HDF5_INT4
#elif   CCTK_INTEGER_PRECISION_2
#define HDF5_INT    HDF5_INT2
#elif   CCTK_INTEGER_PRECISION_1
#define HDF5_INT    HDF5_INT1
#endif

#endif